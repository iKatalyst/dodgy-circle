﻿namespace KatalystKreations
{
    using System;
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(AudioTriggerEvent))]
    [CanEditMultipleObjects]
    public class AudioTriggerEventEditor : Editor
    {
        private AudioTriggerEvent _trigger;
        private Collider _col;
        private Collider2D _col2D;

        private void OnEnable()
        {
            _trigger = (AudioTriggerEvent)target;
        }

        public override void OnInspectorGUI()
        {
            if (_trigger == null)
                return;

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Trigger Mode: ", GUILayout.Width(150));
            _trigger.triggerMode = (AudioTriggerMode)EditorGUILayout.EnumPopup(_trigger.triggerMode);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Loop: ", GUILayout.Width(150));
            _trigger.loop = EditorGUILayout.Toggle(_trigger.loop);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            if (_trigger.loop == true)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField(" Interval:", GUILayout.Width(150));
                _trigger.interval = EditorGUILayout.FloatField(_trigger.interval, GUILayout.Width(100));

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField(" Count:", GUILayout.Width(150));
                _trigger.count = EditorGUILayout.IntField(_trigger.count, GUILayout.Width(100));

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                switch (_trigger.triggerMode)
                {
                    case AudioTriggerMode.UI:
                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField(" Stop Trigger Event:", GUILayout.Width(150));
                        _trigger.stopTriggerUIType = (AudioTriggerTypesUI)EditorGUILayout.EnumPopup(_trigger.stopTriggerUIType);

                        EditorGUILayout.EndHorizontal();
                        break;

                    case AudioTriggerMode.TwoD:
                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField(" Stop Trigger Event:", GUILayout.Width(150));
                        _trigger.stopTrigger2DType = (AudioTriggerTypes2D)EditorGUILayout.EnumPopup(_trigger.stopTrigger2DType);

                        EditorGUILayout.EndHorizontal();
                        break;

                    case AudioTriggerMode.ThreeD:
                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField(" Stop Trigger Event:", GUILayout.Width(150));
                        _trigger.stopTrigger3DType = (AudioTriggerTypes3D)EditorGUILayout.EnumPopup(_trigger.stopTrigger3DType);

                        EditorGUILayout.EndHorizontal();
                        break;

                    case AudioTriggerMode.OnStart:
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                EditorGUILayout.Space();
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Randomize Volume: ", GUILayout.Width(150));
            _trigger.randomizedVolume = EditorGUILayout.Toggle(_trigger.randomizedVolume);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            if (_trigger.randomizedVolume == true)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = 40f;
                _trigger.randomizedVolumeRangeMin = EditorGUILayout.FloatField("Min:", _trigger.randomizedVolumeRangeMin, GUILayout.Width(100));
                _trigger.randomizedVolumeRangeMax = EditorGUILayout.FloatField("Max:", _trigger.randomizedVolumeRangeMax, GUILayout.Width(100));

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("0.5", GUILayout.Width(25));
                EditorGUILayout.MinMaxSlider(ref _trigger.randomizedVolumeRangeMin, ref _trigger.randomizedVolumeRangeMax, 0.5f, 1f);
                EditorGUILayout.LabelField("1", GUILayout.Width(15));

                EditorGUILayout.EndHorizontal();

                if (_trigger.randomizedVolumeRangeMin < 0.5f)
                    _trigger.randomizedVolumeRangeMin = 0.5f;
                if (_trigger.randomizedVolumeRangeMax > 1)
                    _trigger.randomizedVolumeRangeMax = 1;

                EditorGUILayout.Space();
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Randomize Pitch: ", GUILayout.Width(150));
            _trigger.randomizedPitch = EditorGUILayout.Toggle(_trigger.randomizedPitch);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            if (_trigger.randomizedPitch == true)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = 40f;
                _trigger.randomizedPitchRangeMin = EditorGUILayout.FloatField("Min:", _trigger.randomizedPitchRangeMin, GUILayout.Width(100));
                _trigger.randomizedPitchRangeMax = EditorGUILayout.FloatField("Max:", _trigger.randomizedPitchRangeMax, GUILayout.Width(100));

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("-3", GUILayout.Width(15));
                EditorGUILayout.MinMaxSlider(ref _trigger.randomizedPitchRangeMin, ref _trigger.randomizedPitchRangeMax, -3f, 3f);
                EditorGUILayout.LabelField("3", GUILayout.Width(15));

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                if (_trigger.randomizedPitchRangeMin < -3)
                    _trigger.randomizedPitchRangeMin = -3;
                if (_trigger.randomizedPitchRangeMax > 3)
                    _trigger.randomizedPitchRangeMax = 3;
            }

            switch (_trigger.triggerMode)
            {
                case AudioTriggerMode.UI:
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Trigger Event: ", GUILayout.Width(150));
                    _trigger.triggerUIType = (AudioTriggerTypesUI)EditorGUILayout.EnumPopup(_trigger.triggerUIType);

                    EditorGUILayout.EndHorizontal();
                    break;

                case AudioTriggerMode.TwoD:
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Trigger Event: ", GUILayout.Width(150));
                    _trigger.trigger2DType = (AudioTriggerTypes2D)EditorGUILayout.EnumPopup(_trigger.trigger2DType);

                    EditorGUILayout.EndHorizontal();

                    if (_trigger.trigger2DType != AudioTriggerTypes2D.None && _trigger.trigger2DType != AudioTriggerTypes2D.OnGameEvent)
                    {
                        if (_col2D == null)
                        {
                            _col2D = _trigger.GetComponent<Collider2D>();
                            if (_col2D == null)
                            {
                                EditorGUILayout.HelpBox("No Collider detected on this GameObject", MessageType.Error);
                            }
                        }
                    }

                    break;

                case AudioTriggerMode.ThreeD:
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Trigger Event: ", GUILayout.Width(150));
                    _trigger.trigger3DType = (AudioTriggerTypes3D)EditorGUILayout.EnumPopup(_trigger.trigger3DType);

                    EditorGUILayout.EndHorizontal();

                    if (_trigger.trigger3DType != AudioTriggerTypes3D.None && _trigger.trigger3DType != AudioTriggerTypes3D.OnGameEvent)
                    {
                        if (_col == null)
                        {
                            _col = _trigger.GetComponent<Collider>();
                            if (_col == null)
                            {
                                EditorGUILayout.HelpBox("No Collider detected on this GameObject", MessageType.Error);
                            }
                        }
                    }

                    break;

                case AudioTriggerMode.OnStart:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Trigger Tag: ", GUILayout.Width(150));
            _trigger.triggeringTag = EditorGUILayout.TextField(_trigger.triggeringTag);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("End of Play Destruction: ", GUILayout.Width(150));
            _trigger.destroyAfterPlay = (DestroyType)EditorGUILayout.EnumPopup(_trigger.destroyAfterPlay);

            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck() == true)
            {
                switch (_trigger.triggerMode)
                {
                    case AudioTriggerMode.UI:
                        _trigger.trigger2DType = AudioTriggerTypes2D.None;
                        _trigger.trigger3DType = AudioTriggerTypes3D.None;
                        if (_trigger.triggerUIType != AudioTriggerTypesUI.OnGameEvent)
                        {
                            if (_trigger.GetComponent<GameEventListener>() != null)
                            {
                                DestroyImmediate(_trigger.GetComponent<GameEventListener>());
                            }
                        }
                        _col = null;
                        _col2D = null;
                        break;

                    case AudioTriggerMode.TwoD:
                        _trigger.triggerUIType = AudioTriggerTypesUI.None;
                        _trigger.trigger3DType = AudioTriggerTypes3D.None;
                        if (_trigger.trigger2DType != AudioTriggerTypes2D.OnGameEvent)
                        {
                            if (_trigger.GetComponent<GameEventListener>() != null)
                            {
                                DestroyImmediate(_trigger.GetComponent<GameEventListener>());
                            }
                        }
                        _col = null;
                        break;

                    case AudioTriggerMode.ThreeD:
                        _trigger.triggerUIType = AudioTriggerTypesUI.None;
                        _trigger.trigger2DType = AudioTriggerTypes2D.None;
                        if (_trigger.trigger3DType != AudioTriggerTypes3D.OnGameEvent)
                        {
                            if (_trigger.GetComponent<GameEventListener>() != null)
                            {
                                DestroyImmediate(_trigger.GetComponent<GameEventListener>());
                            }
                        }
                        _col2D = null;
                        break;

                    case AudioTriggerMode.OnStart:
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (_trigger.GetComponent<GameEventListener>() != null ||
                    (_trigger.triggerUIType != AudioTriggerTypesUI.OnGameEvent &&
                     _trigger.trigger2DType != AudioTriggerTypes2D.OnGameEvent &&
                     _trigger.trigger3DType != AudioTriggerTypes3D.OnGameEvent)) return;

                var listener = _trigger.gameObject.AddComponent<GameEventListener>();
            }
        }
    }
}