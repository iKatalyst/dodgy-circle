﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace KatalystKreations.BuildPush {

    public class BuildPushEditor : EditorWindow {
        public BuildPushData data;
        public bool build, push, buildPush;

        private const string dataPath = "Data Path";
        private const string dataName = "Data(do not rename).asset";
        private const string targetPlatform = "Target Platform";
        private Regex noWhitespace = new Regex("[^a-zA-Z0-9]+");
        private Regex noLetters = new Regex(@"[^\d]");
        private Texture2D logo;
        private Rect buttonRect;

        public SerializedObject obj;

        private SerializedProperty buildPath;
        private SerializedProperty versionNumber;
        private SerializedProperty autoIncrementVersion;
        private SerializedProperty customVersion;
        private SerializedProperty customVersionString;
        private SerializedProperty userName;
        private SerializedProperty gameName;
        private SerializedProperty channelName;
        private SerializedProperty butlerFilePath;
        private SerializedProperty verboseMode;
        private SerializedProperty hotfixToggle;
        private SerializedProperty pathToggle;
        private SerializedProperty contentToggle;
        private SerializedProperty expansionToggle;
        private SerializedProperty windows;
        private SerializedProperty windows64;
        private SerializedProperty oSX;
        private SerializedProperty oSX64;
        private SerializedProperty oSXUniversal;
        private SerializedProperty linux;
        private SerializedProperty linux64;
        private SerializedProperty linuxUniversal;
        private SerializedProperty android;
        private SerializedProperty webGL;

        #region Properties

        public string DataPath {
            get {
                if (EditorPrefs.HasKey(dataPath))
                    return EditorPrefs.GetString(dataPath);
                else {
                    return EditorPrefs.GetString(dataPath);
                }
            }

            set {
                EditorPrefs.SetString(dataPath, value);
            }
        }

        public string BuildPath {
            get {
                if (obj != null)
                    return buildPath.stringValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return null;
                }
            }

            set {
                buildPath.stringValue = value;
            }
        }

        public string Username {
            get {
                if (obj != null)
                    return userName.stringValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return null;
                }
            }

            set {
                userName.stringValue = value;
            }
        }

        public string GameName {
            get {
                if (obj != null)
                    return gameName.stringValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return null;
                }
            }

            set {
                string name = noWhitespace.Replace(value, @"-").ToLower();
                gameName.stringValue = name;
                obj.ApplyModifiedProperties();
                obj.Update();
            }
        }

        public string ChannelName {
            get {
                if (obj != null)
                    return channelName.stringValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return null;
                }
            }

            set {
                string name = noWhitespace.Replace(value, @"-").ToLower();
                channelName.stringValue = name;
                obj.ApplyModifiedProperties();
                obj.Update();
            }
        }

        public string ButlerFilePath {
            get {
                if (obj != null)
                    return butlerFilePath.stringValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return null;
                }
            }

            set {
                butlerFilePath.stringValue = value;
            }
        }

        public bool AutoIncrementVersion {
            get {
                if (obj != null)
                    return autoIncrementVersion.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                //Undo.RecordObject(this, "Changed Auto Increment");
                autoIncrementVersion.boolValue = value;
            }
        }

        public bool CustomVersion {
            get {
                if (obj != null)
                    return customVersion.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                customVersion.boolValue = value;
            }
        }

        public TargetPlatform TargetPlatforms {
            get {
                if (EditorPrefs.HasKey(targetPlatform))
                    return (TargetPlatform)EditorPrefs.GetInt(targetPlatform);
                else {
                    return (TargetPlatform)EditorPrefs.GetInt(targetPlatform);
                }
            }

            set {
                EditorPrefs.SetInt(targetPlatform, (int)value);
            }
        }

        public bool Windows {
            get {
                if (obj != null)
                    return windows.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                windows.boolValue = value;
            }
        }

        public bool Windows64 {
            get {
                if (obj != null)
                    return windows64.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                windows64.boolValue = value;
            }
        }

        public bool OSX {
            get {
                if (obj != null)
                    return oSX.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                //Undo.RecordObject(this, "Changed OSX Build");
                oSX.boolValue = value;
            }
        }

        public bool OSX64 {
            get {
                if (obj != null)
                    return oSX64.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                oSX64.boolValue = value;
            }
        }

        public bool OSXUniversal {
            get {
                if (obj != null)
                    return oSXUniversal.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                oSXUniversal.boolValue = value;
            }
        }

        public bool Linux {
            get {
                if (obj != null)
                    return linux.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                linux.boolValue = value;
            }
        }

        public bool Linux64 {
            get {
                if (obj != null)
                    return linux64.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                linux64.boolValue = value;
            }
        }

        public bool LinuxUniversal {
            get {
                if (obj != null)
                    return linuxUniversal.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                linuxUniversal.boolValue = value;
            }
        }

        public bool Android {
            get {
                if (obj != null)
                    return android.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                android.boolValue = value;
            }
        }

        public bool WebGL {
            get {
                if (obj != null)
                    return webGL.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                webGL.boolValue = value;
            }
        }

        public bool VerboseMode {
            get {
                if (obj != null)
                    return verboseMode.boolValue;
                else {
                    UnityEngine.Debug.LogError("No Data file found when trying to access information.");
                    return false;
                }
            }

            set {
                verboseMode.boolValue = value;
            }
        }

        #endregion Properties

        [MenuItem("Tools/Build It, Push It/Settings", false, 5)]
        public static void MenuOpenControls() {
            BuildPushEditor window = (BuildPushEditor)GetWindow(typeof(BuildPushEditor));
            window.minSize = new Vector2(512, 650);
            //window.maxSize = new Vector2(512, 650);
            GUIContent title = new GUIContent();
            title.text = "Build it, Push it";
            window.titleContent = title;
            window.Show();
        }

        [MenuItem("Tools/Build It, Push It/Game Status", false, 4)]
        public static void MenuGameStatus() {
            BuildPushData data = AssetDatabase.LoadAssetAtPath<BuildPushData>(EditorPrefs.GetString(dataPath));
            if (data != null)
                BuildPushUtility.GetStatus(data.butlerFilePath, data.userName, data.gameName);
            else
                UnityEngine.Debug.LogError("No data file found. Please use the Settings screen to configure your initial settings.");
        }

        [MenuItem("Tools/Build It, Push It/Build", false, 1)]
        public static void MenuBuild() {
            BuildPushData data = AssetDatabase.LoadAssetAtPath<BuildPushData>(EditorPrefs.GetString(dataPath));
            if (data != null)
                BuildPushUtility.Build(data);
            else
                UnityEngine.Debug.LogError("No data file found. Please use the Settings screen to configure your initial settings.");
        }

        [MenuItem("Tools/Build It, Push It/Push", false, 2)]
        public static void MenuPush() {
            BuildPushData data = AssetDatabase.LoadAssetAtPath<BuildPushData>(EditorPrefs.GetString(dataPath));
            if (data != null)
                BuildPushUtility.Push(data);
            else
                UnityEngine.Debug.LogError("No data file found. Please use the Settings screen to configure your initial settings.");
        }

        [MenuItem("Tools/Build It, Push It/Build Push", false, 3)]
        public static void MenuBuildPush() {
            BuildPushData data = AssetDatabase.LoadAssetAtPath<BuildPushData>(EditorPrefs.GetString(dataPath));
            if (data != null)
                BuildPushUtility.Build(data, true);
            else
                UnityEngine.Debug.LogError("No data file found. Please use the Settings screen to configure your initial settings.");
        }

        private void OnEnable() {
            if (String.IsNullOrEmpty(DataPath))
                data = CreateData();
            else {
                var d = GetData();
                if (d != null) {
                    data = d;
                }
                else {
                    data = CreateData();
                }
            }

            obj = new SerializedObject(data);

            if (obj != null)
                SerializeProperties();
            else
                UnityEngine.Debug.LogError("No data file found.");

            var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\itch\bin\butler.exe";
            if (File.Exists(appData) == false) {
                butlerFilePath.stringValue = EditorUtility.OpenFilePanel("Select the Butler.exe file", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "exe");
            }
            else {
                butlerFilePath.stringValue = appData;
            }
            var image = AssetDatabase.FindAssets("bipi-logo t:texture2D");
            logo = AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(image[0]));
        }

        private void ChangeBuildPath() {
            var path = EditorUtility.SaveFolderPanel("Choose Location for Built Game", String.IsNullOrEmpty(BuildPath) == false ? BuildPath : "", "");
            if (String.IsNullOrEmpty(path) == false)
                BuildPath = path;
        }

        private void SerializeProperties() {
            buildPath = obj.FindProperty("buildPath");
            versionNumber = obj.FindProperty("versionNumber");
            autoIncrementVersion = obj.FindProperty("autoIncrementVersion");
            customVersion = obj.FindProperty("customVersion");
            customVersionString = obj.FindProperty("customVersionString");
            userName = obj.FindProperty("userName");
            gameName = obj.FindProperty("gameName");
            channelName = obj.FindProperty("channelName");
            butlerFilePath = obj.FindProperty("butlerFilePath");
            verboseMode = obj.FindProperty("verboseMode");
            hotfixToggle = obj.FindProperty("hotfixToggle");
            pathToggle = obj.FindProperty("pathToggle");
            contentToggle = obj.FindProperty("contentToggle");
            expansionToggle = obj.FindProperty("expansionToggle");
            windows = obj.FindProperty("windows");
            windows64 = obj.FindProperty("windows64");
            oSX = obj.FindProperty("OSX");
            oSX64 = obj.FindProperty("OSX64");
            oSXUniversal = obj.FindProperty("OSXUniversal");
            linux = obj.FindProperty("linux");
            linux64 = obj.FindProperty("linux64");
            linuxUniversal = obj.FindProperty("linuxUniversal");
            android = obj.FindProperty("android");
            webGL = obj.FindProperty("webGL");
        }

        private void ClearAllValues() {
            obj = null;
            TargetPlatforms = 0;
            AssetDatabase.DeleteAsset(DataPath);
            DataPath = string.Empty;
            BuildPushEditor window = (BuildPushEditor)GetWindow(typeof(BuildPushEditor));
            window.Close();
        }

        private BuildPushData CreateData() {
            var bpe = AssetDatabase.FindAssets("BuildPushData");
            var path = AssetDatabase.GUIDToAssetPath(bpe[0]);
            path = path.Substring(0, path.Length - (("BuildPushData".Length) + 3)) + dataName;
            DataPath = path;
            BuildPushData asset = CreateInstance<BuildPushData>();
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            return asset;
        }

        private BuildPushData GetData() {
            return AssetDatabase.LoadAssetAtPath<BuildPushData>(DataPath);
        }

        public void StartBuildPush() {
            if (build == true) {
                BuildPushUtility.Build(data);
                return;
            }
            if (push == true) {
                BuildPushUtility.Push(data);
                return;
            }
            if (buildPush == true) {
                BuildPushUtility.Build(data, true);
            }
        }

        private void OnGUI() {
            try {
                if (data == null)
                    return;
                GUILayout.BeginHorizontal();
                GUILayout.Label(logo);
                GUILayout.EndHorizontal();

                GUILayout.Space(20);

                if (String.IsNullOrEmpty(ButlerFilePath)) {
                    GUILayout.Label("No Butler file detected, direct Push2Itch to it.", EditorStyles.boldLabel);
                    if (GUILayout.Button("Find Butler", GUILayout.Width(100))) {
                        ButlerFilePath = EditorUtility.OpenFilePanel("Select the butler.exe file", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "exe");
                    }
                    return;
                }
                if (ButlerFilePath.Substring(ButlerFilePath.Length - 10).ToLower() != "butler.exe") {
                    GUILayout.Label("Invalid file supplied to Push2Itch.", EditorStyles.boldLabel);
                    if (GUILayout.Button("Find Butler", GUILayout.Width(100))) {
                        ButlerFilePath = EditorUtility.OpenFilePanel("Select the butler.exe file", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "exe");
                    }
                    return;
                }

                GUILayout.BeginHorizontal();
                GUILayout.Label(new GUIContent("Itch Username: ", "Your username on the itch.io website."), EditorStyles.boldLabel, GUILayout.Width(100));
                EditorGUILayout.PropertyField(userName, GUIContent.none);
                GUILayout.Space(20);
                if (GUILayout.Button(new GUIContent("Login", "If you have not logged into Butler yet, this will send the command to butler to help you do so."))) {
                    BuildPushUtility.Login(data.butlerFilePath);
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(5);

                GUILayout.BeginHorizontal();
                GUILayout.Label(new GUIContent("Game Name: ", "The name of your game on itch.io. " +
                                                "Special characters and whitespace will be removed/" +
                                                "replaced for you to fit the way Itch wants it sent to them."),
                                                EditorStyles.boldLabel, GUILayout.Width(100));
                EditorGUILayout.PropertyField(gameName, GUIContent.none);
                GameName = gameName.stringValue;
                GUILayout.EndHorizontal();

                GUILayout.Space(5);

                GUILayout.BeginHorizontal();
                GUILayout.Label(new GUIContent("Channel Name: ", "The name of channel you want to upload to on itch.io. " +
                                                "Special characters and whitespace will be removed/" +
                                                "replaced for you to fit the way Itch wants it sent to them."),
                                                EditorStyles.boldLabel, GUILayout.Width(100));
                EditorGUILayout.PropertyField(channelName, GUIContent.none);
                ChannelName = channelName.stringValue;
                GUILayout.Space(50);

                EditorGUILayout.HelpBox("The platform will be added to this, so if this is an alpha build for windows, " +
                                         "just type alpha and it'll end up as 'windows-alpha' on itch.", MessageType.Info);

                GUILayout.EndHorizontal();

                GUILayout.Space(25);
                EditorGUILayout.PropertyField(customVersion, new GUIContent("Custom Version?", "Define a custom version for your game if you do not want to use the auto-incrementor."), GUILayout.Width(175));
                if (CustomVersion == true) {
                    EditorGUILayout.PropertyField(customVersionString, GUIContent.none, GUILayout.Width(100));
                    AutoIncrementVersion = false;
                }
                else {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(autoIncrementVersion, new GUIContent("Auto Increment Version?", "This will automagically increment your version number just prior to making a build for you."), GUILayout.Width(175));
                    if (AutoIncrementVersion == false)
                        EditorGUILayout.HelpBox("Fill in your own numbers before ticking this box. The version will increment when you make a build incase you need to access the version number in-game, it'll be updated.", MessageType.Warning);
                    else
                        CustomVersion = false;
                    GUILayout.EndHorizontal();
                    GUILayout.Space(25);
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Version: ", "Define the version using the auto-incrementor format. " +
                                                    "The numbers are, from left to right: Expansion, Content, Patch, Hotfix")
                                                    , EditorStyles.boldLabel, GUILayout.Width(65));
                    if (AutoIncrementVersion == false) {
                        EditorGUILayout.PropertyField(versionNumber.FindPropertyRelative("expansion"), GUIContent.none);
                        EditorGUILayout.PropertyField(versionNumber.FindPropertyRelative("content"), GUIContent.none);
                        EditorGUILayout.PropertyField(versionNumber.FindPropertyRelative("patch"), GUIContent.none);
                        EditorGUILayout.PropertyField(versionNumber.FindPropertyRelative("hotfix"), GUIContent.none);
                    }
                    GUILayout.Space(5);
                    GUILayout.Label(data.versionNumber.ReturnVersion(), GUILayout.Width(100));
                    GUILayout.EndHorizontal();
                }

                GUILayout.Space(20);

                GUILayout.BeginHorizontal();
                GUILayout.Space(25);
                if (GUILayout.Button(new GUIContent("Change Build Path", "Change the path where Build It, Push It " +
                                                    "builds to and looks to when pushing a build."), GUILayout.Width(150))) {
                    ChangeBuildPath();
                }

                GUILayout.Space(20);

                if (String.IsNullOrEmpty(BuildPath) == true) {
                    EditorGUILayout.HelpBox("No build path currently selected.", MessageType.Error);
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(20);

                #region Display Build Paths

                GUILayout.BeginHorizontal();
                GUILayout.Label("Base Directory: ", EditorStyles.boldLabel, GUILayout.Width(120));
                GUILayout.Label(buildPath.stringValue);
                GUILayout.EndHorizontal();

                if ((TargetPlatforms & TargetPlatform.Windows) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("Windows: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/Windows");
                    GUILayout.EndHorizontal();
                    windows.boolValue = true;
                }
                else
                    windows.boolValue = false;

                if ((TargetPlatforms & TargetPlatform.Windows64) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("Windows64: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/Windows64");
                    GUILayout.EndHorizontal();
                    Windows64 = true;
                }
                else
                    Windows64 = false;

                if ((TargetPlatforms & TargetPlatform.OSX) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("OSX: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/OSX");
                    GUILayout.EndHorizontal();
                    OSX = true;
                }
                else
                    OSX = false;

                if ((TargetPlatforms & TargetPlatform.OSX64) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("OSX64: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/OSX64");
                    GUILayout.EndHorizontal();
                    OSX64 = true;
                }
                else
                    OSX64 = false;

                if ((TargetPlatforms & TargetPlatform.OSXUniversal) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("OSXUniversal: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/OSXUniversal");
                    GUILayout.EndHorizontal();
                    OSXUniversal = true;
                }
                else
                    OSXUniversal = false;

                if ((TargetPlatforms & TargetPlatform.Linux) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("Linux: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/Linux");
                    GUILayout.EndHorizontal();
                    Linux = true;
                }
                else
                    Linux = false;

                if ((TargetPlatforms & TargetPlatform.Linux64) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("Linux64: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/Linux64");
                    GUILayout.EndHorizontal();
                    Linux64 = true;
                }
                else
                    Linux64 = false;

                if ((TargetPlatforms & TargetPlatform.LinuxUniversal) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("LinuxUniversal: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/LinuxUniversal");
                    GUILayout.EndHorizontal();
                    LinuxUniversal = true;
                }
                else
                    LinuxUniversal = false;

                if ((TargetPlatforms & TargetPlatform.Android) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("Android: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/Android");
                    GUILayout.EndHorizontal();
                    Android = true;
                }
                else
                    Android = false;

                if ((TargetPlatforms & TargetPlatform.WebGL) > 0) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(5);
                    GUILayout.Label("WebGL: ", EditorStyles.boldLabel, GUILayout.Width(120));
                    GUILayout.Label(BuildPath + @"/WebGL");
                    GUILayout.EndHorizontal();
                    WebGL = true;
                }
                else
                    WebGL = false;

                #endregion Display Build Paths

                GUILayout.Space(20);

                TargetPlatforms = (TargetPlatform)EditorGUILayout.EnumMaskField(new GUIContent("Build Targets: ",
                                  "Choose which platforms you'd like to build/push to."), TargetPlatforms, GUILayout.Width(350));

                GUILayout.Space(25);

                GUILayout.BeginHorizontal();
                GUILayout.Space(20);
                if (GUILayout.Button(new GUIContent("Build", "Make a build of all selected target platforms."), GUILayout.Width(100))) {
                    build = true;
                    push = false;
                    buildPush = false;
                    if (AutoIncrementVersion == true)
                        PopupWindow.Show(buttonRect, new BuildPushPopup());
                    else
                        BuildPushUtility.Build(data);
                }
                if (Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition)) {
                    buttonRect = GUILayoutUtility.GetLastRect();
                }

                GUILayout.Space(15);

                if (GUILayout.Button(new GUIContent("Push", "Push any selected platform builds found " +
                                                    "in the build directory to Itch."), GUILayout.Width(100))) {
                    build = false;
                    push = true;
                    buildPush = false;
                    BuildPushUtility.Push(data);
                }
                if (Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
                    buttonRect = GUILayoutUtility.GetLastRect();

                GUILayout.Space(15);

                if (GUILayout.Button(new GUIContent("Build and Push", "This will make a build of a selected platform, " +
                                                    "then begin pushing it to itch while working on the next build if " +
                                                    "multiple platforms were selected, then repeat until all selected " +
                                                    "platforms are done."), GUILayout.Width(100))) {
                    build = false;
                    push = false;
                    buildPush = true;
                    if (AutoIncrementVersion == true)
                        PopupWindow.Show(buttonRect, new BuildPushPopup());
                    else {
                        BuildPushUtility.Build(data, buildPush);
                    }
                }
                if (Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
                    buttonRect = GUILayoutUtility.GetLastRect();

                GUILayout.Space(15);

                if (GUILayout.Button(new GUIContent("Build Status", "Will open a cmd prompt and access that status information of your current games builds."), GUILayout.Width(100))) {
                    BuildPushUtility.GetStatus(ButlerFilePath, Username, GameName);
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(20);

                GUILayout.BeginHorizontal();
                GUILayout.Space(50);
                var defaultColor = GUI.color;
                GUI.color = Color.red;
                if (GUILayout.Button("Clear Values", GUILayout.Width(400)))
                    if (EditorUtility.DisplayDialog("Clear Values?", "Are you sure you want to clear all information? This will close the Build It, Push It window after the reset.", "Yes, clear it", "No, don't clear")) {
                        ClearAllValues();
                        return;
                    }
                GUILayout.EndHorizontal();

                GUI.color = defaultColor;

                //GUILayout.BeginHorizontal();
                //GUILayout.Space(25);
                //EditorGUILayout.HelpBox("Ignore the null reference exceptions thrown by this script right after a build/push, it happens due to the build/push process holding up Unity's thread while this window is trying to paint itself.", MessageType.Info);
                //GUILayout.EndHorizontal();

                GUILayout.Space(5);
                GUILayout.BeginHorizontal();
                VerboseMode = GUILayout.Toggle(VerboseMode, new GUIContent("Verbose Mode", "Makes Build It, Push It say everything it is doing in the console. Helpful if debugging a problem that arises."), GUILayout.Width(125));
                GUILayout.Space(100);
                GUILayout.Label("Created by Katalyst Kreations", EditorStyles.boldLabel);
                GUILayout.EndHorizontal();
                if (obj != null) {
                    obj.ApplyModifiedProperties();
                    obj.Update();
                }
            }
            catch {
            }
        }
    }
}