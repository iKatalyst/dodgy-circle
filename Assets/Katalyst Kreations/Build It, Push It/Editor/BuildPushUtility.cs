﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UnityEditor;

namespace KatalystKreations.BuildPush {

    public static class BuildPushUtility {

        public static void Build(BuildPushData data, bool buildPush = false) {
            if (EditorApplication.isCompiling || EditorApplication.isPaused || EditorApplication.isPlaying || EditorApplication.isUpdating) {
                UnityEngine.Debug.LogWarning("The editor is currently busy with a task or in play mode. Please wait or exit play mode.");
                return;
            }
            if (String.IsNullOrEmpty(data.buildPath))
                data.buildPath = EditorUtility.SaveFolderPanel("Choose Location for Built Game:", "", "");

            if (String.IsNullOrEmpty(data.userName)) {
                UnityEngine.Debug.LogError("Build It, Push It : Username empty.");
                return;
            }
            if (String.IsNullOrEmpty(data.gameName)) {
                UnityEngine.Debug.LogError("Build It, Push It : Game Name empty.");
                return;
            }
            if (String.IsNullOrEmpty(data.channelName)) {
                UnityEngine.Debug.LogError("Build It, Push It : Channel Name empty.");
                return;
            }

            if (data.autoIncrementVersion == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Auto-Incrementing the version. Starting value: " + data.versionNumber.ReturnVersion());
                if (data.hotfixToggle == true) {
                    data.versionNumber.hotfix++;
                    if (data.verboseMode == true)
                        UnityEngine.Debug.Log("Hotfix version incremented.");
                }
                if (data.patchToggle == true) {
                    data.versionNumber.patch++;
                    data.versionNumber.hotfix = 0;
                    if (data.verboseMode == true)
                        UnityEngine.Debug.Log("Patch version incremented. Hotfix set to 0.");
                }
                if (data.contentToggle == true) {
                    data.versionNumber.content++;
                    data.versionNumber.hotfix = 0;
                    data.versionNumber.patch = 0;
                    if (data.verboseMode == true)
                        UnityEngine.Debug.Log("Content version incremented. Patch and Hotfox set back to 0.");
                }
                if (data.expansionToggle == true) {
                    data.versionNumber.expansion++;
                    data.versionNumber.content = 0;
                    data.versionNumber.hotfix = 0;
                    data.versionNumber.patch = 0;
                    if (data.verboseMode == true)
                        UnityEngine.Debug.Log("Expansion version incremented. Content, patch, and hotfix set back to 0.");
                }

                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Auto-Incrementing complete. New value: " + data.versionNumber.ReturnVersion());
            }

            if (data.windows == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting Windows Build proccess...");
                CommenceBuild(data, "/Windows", BuildTarget.StandaloneWindows, ".exe", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished Windows Build process...");
            }

            if (data.windows64 == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting Windows64 Build proccess...");
                CommenceBuild(data, "/Windows64", BuildTarget.StandaloneWindows64, ".exe", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished Windows64 Build proccess...");
            }

            if (data.OSX == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting OSX Build proccess...");
                CommenceBuild(data, "/OSX", BuildTarget.StandaloneOSXIntel, ".app", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished OSX Build proccess...");
            }

            if (data.OSX64 == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting OSX64 Build proccess...");
                CommenceBuild(data, "/OSX64", BuildTarget.StandaloneOSXIntel64, ".app", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished OSX64 Build proccess...");
            }

            if (data.OSXUniversal == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting OSXUniversal Build proccess...");
                CommenceBuild(data, "/OSXUniversal", BuildTarget.StandaloneOSX, ".app", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished OSXUniversal Build proccess...");
            }

            if (data.linux == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting Linux Build proccess...");
                CommenceBuild(data, "/Linux", BuildTarget.StandaloneLinux, ".x86", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished Linux Build proccess...");
            }

            if (data.linux64 == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting Linux64 Build proccess...");
                CommenceBuild(data, "/Linux64", BuildTarget.StandaloneLinux64, ".x64", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished Linux64 Build proccess...");
            }

            if (data.linuxUniversal == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting LinuxUniversal Build proccess...");
                CommenceBuild(data, "/LinuxUniversal", BuildTarget.StandaloneLinuxUniversal, ".x86_x64", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished LinuxUniversal Build proccess...");
            }

            if (data.android == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting Android Build proccess...");
                CommenceBuild(data, "/Android", BuildTarget.Android, ".apk", buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished Android Build proccess...");
            }

            if (data.webGL == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Starting WebGL Build proccess...");
                CommenceBuild(data, "/WebGL", BuildTarget.WebGL, string.Empty, buildPush);
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Finished WebGL Build proccess...");
            }
        }

        private static void CommenceBuild(BuildPushData data, string platformString, BuildTarget target, string extension, bool buildPush = false) {
            Stopwatch timer = new Stopwatch();
            Directory.CreateDirectory(data.buildPath + @platformString);
            if (data.verboseMode == true)
                UnityEngine.Debug.Log(string.Format("Build directory found/created. -- {0}", data.buildPath + @platformString));
#if UNITY_5_5 || UNITY_5_6
            BuildPlayerOptions options = new BuildPlayerOptions();
            options.target = target;
            options.scenes = SceneNames();
            options.options = BuildOptions.ShowBuiltPlayer;
            options.locationPathName = data.buildPath + @platformString + "/" + PlayerSettings.productName + @extension;
            if (data.verboseMode == true) {
                UnityEngine.Debug.Log(string.Format("Unity 5.5 -- Build options configured as Target: {0} -- Scenes: {1} -- Options: {2}",
                    Enum.GetName(typeof(BuildTarget), target), String.Join(", ", SceneNames()), Enum.GetName(typeof(BuildOptions),
                    options.options)));
                UnityEngine.Debug.Log("Starting build...");
                timer.Start();
            }
            BuildPipeline.BuildPlayer(options);
#else
            var path = data.buildPath + @platformString + "/" + PlayerSettings.productName + @extension;
            if (data.verboseMode == true) {
                UnityEngine.Debug.Log(string.Format("Build options configured as Target: {0} -- Scenes: {1} -- Options: {2}",
                    Enum.GetName(typeof(BuildTarget), target), String.Join(", ", SceneNames()), Enum.GetName(typeof(BuildOptions),
                    BuildOptions.ShowBuiltPlayer)));
                UnityEngine.Debug.Log("Starting build...");
                timer.Start();
            }
            BuildPipeline.BuildPlayer(SceneNames(), path, target, BuildOptions.ShowBuiltPlayer);
#endif
            while (BuildPipeline.isBuildingPlayer == true) { }
            if (data.verboseMode == true) {
                timer.Stop();
                UnityEngine.Debug.Log(string.Format("Build complete. Completion time: {0} seconds.", timer.ElapsedMilliseconds / 1000));
            }
            if (data.verboseMode == true)
                UnityEngine.Debug.Log("Build Push status: " + buildPush);

            if (buildPush == true) {
                if (data.verboseMode == true)
                    UnityEngine.Debug.Log("Build Push was assigned, moving to Itch push.");
                Push(data, target);
            }
        }

#if UNITY_5_5 || UNITY_5_6

        public static void Push(BuildPushData data, BuildTarget target = BuildTarget.NoTarget) {
            if (EditorApplication.isCompiling || EditorApplication.isPaused || EditorApplication.isPlaying || EditorApplication.isUpdating) {
                UnityEngine.Debug.Log("The editor is currently busy with a task or in play mode. We please wait or exit play mode.");
                return;
            }
            var path = data.buildPath.Replace('/', '\\');
            if (data.verboseMode == true)
                UnityEngine.Debug.Log("Build path is: " + path + " -- Selecting it to push to Itch.");

            if (target == BuildTarget.NoTarget) {
                if (data.windows == true) {
                    CommencePush(data, path, @"\Windows", "windows-");
                }

                if (data.windows64 == true) {
                    CommencePush(data, path, @"\Windows64", "windows64-");
                }

                if (data.OSX == true) {
                    CommencePush(data, path, @"\OSX", "osx-");
                }

                if (data.OSX64 == true) {
                    CommencePush(data, path, @"\OSX64", "osx64-");
                }

                if (data.OSXUniversal == true) {
                    CommencePush(data, path, @"\OSXUniversal", "osxuniversal-");
                }

                if (data.linux == true) {
                    CommencePush(data, path, @"\Linux", "linux-");
                }

                if (data.linux64 == true) {
                    CommencePush(data, path, @"\Linux64", "linux64-");
                }

                if (data.linuxUniversal == true) {
                    CommencePush(data, path, @"\LinuxUniversal", "linuxuniversal-");
                }

                if (data.android == true) {
                    CommencePush(data, path, @"\Android", "android-");
                }

                if (data.webGL == true) {
                    CommencePush(data, path, @"\WebGL", "webgl-");
                }
            }
            else {
                if (target == BuildTarget.StandaloneWindows) {
                    CommencePush(data, path, @"\Windows", "windows-");
                }

                if (target == BuildTarget.StandaloneWindows64) {
                    CommencePush(data, path, @"\Windows64", "windows64-");
                }

                if (target == BuildTarget.StandaloneOSXIntel) {
                    CommencePush(data, path, @"\OSX", "osx-");
                }

                if (target == BuildTarget.StandaloneOSXIntel64) {
                    CommencePush(data, path, @"\OSX64", "osx64-");
                }

                if (target == BuildTarget.StandaloneOSXUniversal) {
                    CommencePush(data, path, @"\OSXUniversal", "osxuniversal-");
                }

                if (target == BuildTarget.StandaloneLinux) {
                    CommencePush(data, path, @"\Linux", "linux-");
                }

                if (target == BuildTarget.StandaloneLinux64) {
                    CommencePush(data, path, @"\Linux64", "linux64-");
                }

                if (target == BuildTarget.StandaloneLinuxUniversal) {
                    CommencePush(data, path, @"\LinuxUniversal", "linuxuniversal-");
                }

                if (target == BuildTarget.Android) {
                    CommencePush(data, path, @"\Android", "android-");
                }

                if (target == BuildTarget.WebGL) {
                    CommencePush(data, path, @"\WebGL", "webgl-");
                }
            }
        }

#else
        public static void Push(BuildPushData data, BuildTarget target = BuildTarget.SamsungTV) {
            if (EditorApplication.isCompiling || EditorApplication.isPaused || EditorApplication.isPlaying || EditorApplication.isUpdating) {
                UnityEngine.Debug.Log("The editor is currently busy with a task or in play mode. We please wait or exit play mode.");
                return;
            }
            var path = data.buildPath.Replace('/', '\\');
            if (data.verboseMode == true)
                UnityEngine.Debug.Log("Build path is: " + path + " -- Selecting it to push to Itch.");

            if (target == BuildTarget.SamsungTV) {
                if (data.windows == true) {
                    CommencePush(data, path, @"\Windows", "windows-");
                }

                if (data.windows64 == true) {
                    CommencePush(data, path, @"\Windows64", "windows64-");
                }

                if (data.OSX == true) {
                    CommencePush(data, path, @"\OSX", "osx-");
                }

                if (data.OSX64 == true) {
                    CommencePush(data, path, @"\OSX64", "osx64-");
                }

                if (data.OSXUniversal == true) {
                    CommencePush(data, path, @"\OSXUniversal", "osxuniversal-");
                }

                if (data.linux == true) {
                    CommencePush(data, path, @"\Linux", "linux-");
                }

                if (data.linux64 == true) {
                    CommencePush(data, path, @"\Linux64", "linux64-");
                }

                if (data.linuxUniversal == true) {
                    CommencePush(data, path, @"\LinuxUniversal", "linuxuniversal-");
                }

                if (data.android == true) {
                    CommencePush(data, path, @"\Android", "android-");
                }

                if (data.webGL == true) {
                    CommencePush(data, path, @"\WebGL", "webgl-");
                }
            }
            else {
                if (target == BuildTarget.StandaloneWindows) {
                    CommencePush(data, path, @"\Windows", "windows-");
                }

                if (target == BuildTarget.StandaloneWindows64) {
                    CommencePush(data, path, @"\Windows64", "windows64-");
                }

                if (target == BuildTarget.StandaloneOSXIntel) {
                    CommencePush(data, path, @"\OSX", "osx-");
                }

                if (target == BuildTarget.StandaloneOSXIntel64) {
                    CommencePush(data, path, @"\OSX64", "osx64-");
                }

                if (target == BuildTarget.StandaloneOSX) {
                    CommencePush(data, path, @"\OSXUniversal", "osxuniversal-");
                }

                if (target == BuildTarget.StandaloneLinux) {
                    CommencePush(data, path, @"\Linux", "linux-");
                }

                if (target == BuildTarget.StandaloneLinux64) {
                    CommencePush(data, path, @"\Linux64", "linux64-");
                }

                if (target == BuildTarget.StandaloneLinuxUniversal) {
                    CommencePush(data, path, @"\LinuxUniversal", "linuxuniversal-");
                }

                if (target == BuildTarget.Android) {
                    CommencePush(data, path, @"\Android", "android-");
                }

                if (target == BuildTarget.WebGL) {
                    CommencePush(data, path, @"\WebGL", "webgl-");
                }
            }
        }

#endif

        private static void CommencePush(BuildPushData data, string path, string platform, string buildType) {
            Process cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.RedirectStandardInput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            if (data.verboseMode == true) {
                UnityEngine.Debug.Log("-----THIS WILL NOT OPEN A COMMAND PROMPT WINDOW! INSTEAD, ALL OUTPUT WILL BE ROUTED TO UNITY'S CONSOLE. IT WILL TELL YOU WHEN IT IS DONE PUSHING, GIVE IT TIME-----");
                cmdProcess.StartInfo.CreateNoWindow = true;
                cmdProcess.StartInfo.RedirectStandardError = true;
                cmdProcess.StartInfo.RedirectStandardOutput = true;
                cmdProcess.OutputDataReceived += new DataReceivedEventHandler((x, y) => {
                    if (String.IsNullOrEmpty(y.Data) == false)
                        UnityEngine.Debug.Log(y.Data);
                });
                cmdProcess.ErrorDataReceived += new DataReceivedEventHandler((x, y) => {
                    if (String.IsNullOrEmpty(y.Data) == false)
                        UnityEngine.Debug.LogError(y.Data);
                });
                UnityEngine.Debug.Log(string.Format("Starting command prompt with parameters -- Redirect Standard Input: {0} -- Use Shell Execute: {1}", cmdProcess.StartInfo.RedirectStandardInput, cmdProcess.StartInfo.UseShellExecute));
            }
            var argument = string.Format("\"{0}\" push \"{1}{2}\" {3}/{4}:{5}{6} --userversion {7}",
                                             data.butlerFilePath, path, platform, data.userName, data.gameName, buildType, data.channelName,
                                            (data.customVersion ? data.customVersionString : data.versionNumber.ReturnVersion()));
            cmdProcess.Start();
            if (data.verboseMode == true) {
                cmdProcess.BeginErrorReadLine();
                cmdProcess.BeginOutputReadLine();
                UnityEngine.Debug.Log("Sending commands to the command prompt.");
                UnityEngine.Debug.Log(argument);
            }
            StreamWriter myStream = new StreamWriter(cmdProcess.StandardInput.BaseStream, Encoding.ASCII);
            myStream.WriteLine(argument);
            myStream.Flush();
            if (data.verboseMode == true)
                cmdProcess.StandardInput.Close();
        }

        public static void GetStatus(string butlerPath, string username, string gamename) {
            if (EditorApplication.isCompiling || EditorApplication.isPaused || EditorApplication.isPlaying || EditorApplication.isUpdating) {
                UnityEngine.Debug.Log("The editor is currently busy with a task or in play mode. We please wait or exit play mode.");
                return;
            }
            Process cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.RedirectStandardInput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.Start();
            StreamWriter myStream = new StreamWriter(cmdProcess.StandardInput.BaseStream, Encoding.ASCII);
            myStream.WriteLine('"' + butlerPath + '"' + " status " + username + "/" + gamename);
            myStream.Flush();
            cmdProcess.WaitForExit();
        }

        public static void Login(string butlerPath) {
            if (EditorApplication.isCompiling || EditorApplication.isPaused || EditorApplication.isPlaying || EditorApplication.isUpdating) {
                UnityEngine.Debug.Log("The editor is currently busy with a task or in play mode. We please wait or exit play mode.");
                return;
            }
            Process cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.RedirectStandardInput = true;
            cmdProcess.StartInfo.UseShellExecute = false;
            cmdProcess.Start();
            StreamWriter myStream = new StreamWriter(cmdProcess.StandardInput.BaseStream, Encoding.ASCII);
            myStream.WriteLine(string.Format("\"{0}\" login", butlerPath));
            myStream.Flush();
            cmdProcess.WaitForExit();
        }

        public static string[] SceneNames() {
            List<string> temp = new List<string>();
            foreach (EditorBuildSettingsScene S in EditorBuildSettings.scenes) {
                if (S.enabled) {
                    temp.Add(S.path);
                }
            }
            return temp.ToArray();
        }
    }
}