﻿[System.Serializable]
public class Int64Reference
{
    public bool UseConstant = true;
    public long ConstantValue;
    public Int64Variable Variable;

    public Int64Reference()
    {
    }

    public Int64Reference(long value)
    {
        UseConstant = true;
        ConstantValue = value;
    }

    public long Value
    {
        get { return UseConstant ? ConstantValue : Variable.Value; }
    }

    public static implicit operator long(Int64Reference reference)
    {
        return reference.Value;
    }
}