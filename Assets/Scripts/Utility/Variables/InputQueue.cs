﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InputQueue : ScriptableObject
{
    public List<Vector3> CurrentInput;
}