﻿using UnityEngine;

[CreateAssetMenu]
public class Int64Variable : ScriptableObject
{
#if UNITY_EDITOR

    [Multiline]
    public string DeveloperDescription = "";
#endif

    public long Value;

    public void SetValue(long value)
    {
        Value = value;
    }

    public void SetValue(Int64Variable value)
    {
        Value = value.Value;
    }

    public void ApplyChange(long amount)
    {
        Value += amount;
    }

    public void ApplyChange(Int64Variable amount)
    {
        Value += amount.Value;
    }
}