﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerRegion : MonoBehaviour
{
    public ShapeMoveDirection direction;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        switch (direction)
        {
            case ShapeMoveDirection.NONE:
                break;

            case ShapeMoveDirection.UP:
                Gizmos.DrawLine(new Vector3(transform.position.x - 5, transform.position.y, transform.position.z), new Vector3(transform.position.x + 5, transform.position.y, transform.position.z));
                break;

            case ShapeMoveDirection.DOWN:
                Gizmos.DrawLine(new Vector3(transform.position.x - 5, transform.position.y, transform.position.z), new Vector3(transform.position.x + 5, transform.position.y, transform.position.z));
                break;

            case ShapeMoveDirection.LEFT:
                Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y - 5, transform.position.z), new Vector3(transform.position.x, transform.position.y + 5, transform.position.z));
                break;

            case ShapeMoveDirection.RIGHT:
                Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y - 5, transform.position.z), new Vector3(transform.position.x, transform.position.y + 5, transform.position.z));
                break;
        }
    }
}