﻿using UnityEngine;
using TMPro;
using DG.Tweening;

[RequireComponent(typeof(TextMeshProUGUI))]
public class BlinkText : MonoBehaviour
{
    public float blinkSpeed;
    public bool beginOnStart = true;
    private TextMeshProUGUI words;
    private Color newColor;
    private Tweener tween;

    private void Start()
    {
        words = GetComponent<TextMeshProUGUI>();
        newColor = new Color(words.color.r, words.color.g, words.color.b, 0f);
        if (beginOnStart == true)
            tween = words.DOColor(newColor, blinkSpeed).SetLoops(-1, LoopType.Yoyo);
    }

    public void StartBlink()
    {
        tween.Complete();
        tween = words.DOColor(newColor, blinkSpeed).SetLoops(-1, LoopType.Yoyo);
    }
}