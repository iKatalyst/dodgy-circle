﻿using UnityEngine;
using TMPro;

public class TutorialModeDisplay : MonoBehaviour
{
    [Multiline]
    public string realTimeControls;

    [Multiline]
    public string stepBasedControls;

    public ActiveGameMode mode;

    private TextMeshProUGUI words;

    private void Start()
    {
        words = GetComponent<TextMeshProUGUI>();

        if (mode.CurrentMode == GameMode.REAL_TIME)
        {
            words.text = realTimeControls;
        }
        else
        {
            words.text = stepBasedControls;
        }
    }
}