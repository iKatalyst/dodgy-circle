﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SliderAdjustValue : MonoBehaviour
{
    public AudioMixer mixerGroup;
    public Slider slider;

    private const string volumeName = "MusicVolume";

    private void Start()
    {
        slider.value = PlayerPrefs.HasKey(volumeName) ? PlayerPrefs.GetFloat(volumeName) : -20f;
        mixerGroup.SetFloat(volumeName, PlayerPrefs.HasKey(volumeName) ? PlayerPrefs.GetFloat(volumeName) : -20f);
        slider.onValueChanged.AddListener((x) =>
        {
            PlayerPrefs.SetFloat(volumeName, x);
            mixerGroup.SetFloat(volumeName, x);
        });
    }
}