﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleRealTime : MonoBehaviour
{
    public ActiveGameMode realTimeControl;

    private Toggle toggle;

    private void Start()
    {
        toggle = GetComponent<Toggle>();
        realTimeControl.CurrentMode = (GameMode)PlayerPrefs.GetInt("RealTime");
        if (realTimeControl.CurrentMode == GameMode.REAL_TIME)
            toggle.isOn = true;
        else
            toggle.isOn = false;
    }

    public void ToggleControls()
    {
        if (toggle.isOn == true)
        {
            realTimeControl.CurrentMode = GameMode.REAL_TIME;
            PlayerPrefs.SetInt("RealTime", (int)realTimeControl.CurrentMode);
        }
        else
        {
            realTimeControl.CurrentMode = GameMode.STEP_BASED;
            PlayerPrefs.SetInt("RealTime", (int)realTimeControl.CurrentMode);
        }
    }
}