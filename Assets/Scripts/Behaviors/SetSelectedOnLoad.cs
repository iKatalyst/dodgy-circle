﻿using UnityEngine;
using UnityEngine.UI;

public class SetSelectedOnLoad : MonoBehaviour
{
    public bool selectOnStart;
    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();
        if (selectOnStart == true)
            SetSelected();
    }

    public void SetSelected()
    {
        if (button != null)
        {
            button.Select();
        }
    }
}