﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class DisplayIntVariable : MonoBehaviour
{
    public IntVariable toDisplay;
    public bool useColorCoding;
    public bool invertColors;
    public float warningThreshold;
    public float emergencyThreshold;
    public ColorData colors;

    private TextMeshProUGUI words;

    private void Awake()
    {
        words = GetComponent<TextMeshProUGUI>();
        if (useColorCoding == true)
        {
            if (invertColors == true)
                words.color = colors.red;
            else
                words.color = colors.green;
        }
    }

    private void Update()
    {
        if (useColorCoding == true)
        {
            if (invertColors == true)
            {
                if (toDisplay.Value >= warningThreshold && toDisplay.Value < emergencyThreshold)
                    words.color = colors.yellow;
                if (toDisplay.Value >= emergencyThreshold)
                    words.color = colors.green;
            }
            else
            {
                if (toDisplay.Value <= warningThreshold && toDisplay.Value > emergencyThreshold)
                    words.color = colors.yellow;
                if (toDisplay.Value <= emergencyThreshold)
                    words.color = colors.red;
            }
        }

        words.text = toDisplay.Value.ToString();
    }
}