﻿using UnityEngine;

public class CollectItem : MonoBehaviour
{
    public IntVariable objectCount;
    public GameEvent spawnNext;
    public GameObject collectPrefab;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Collect();
        }
    }

    public void Collect()
    {
        GameObject obj = Instantiate(collectPrefab, transform.position, Quaternion.identity);
        var parts = obj.GetComponent<ParticleSystem>();
        var dur = parts.main.duration + parts.main.startLifetime.constantMin;
        Destroy(obj, dur);
        objectCount.Value--;
        spawnNext.Raise();
        Destroy(gameObject);
    }
}