﻿using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
public class ShapeController : MonoBehaviour
{
    public ActiveGameMode mode;
    public ShapeMoveDirection direction;
    public CurrentLevel moveSpeed;
    public ShapeData shapeData;
    public ActiveShapes activeShapes;
    public GameState state;
    public FloatVariable turnSpeed;
    public ColorData colors;

    private SpriteRenderer sprite;
    private Rigidbody2D rb;
    private float speed;

    public void UpdateData(ShapeData data)
    {
        shapeData = data;
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        if (shapeData != null)
            GetComponent<SpriteRenderer>().sprite = shapeData.image;

        switch (direction)
        {
            case ShapeMoveDirection.UP:
                transform.rotation = Quaternion.Euler(0, 0, 90);
                break;

            case ShapeMoveDirection.DOWN:
                transform.rotation = Quaternion.Euler(0, 0, -90);
                break;

            case ShapeMoveDirection.LEFT:
                transform.rotation = Quaternion.Euler(0, 0, 180);
                break;

            case ShapeMoveDirection.RIGHT:
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
        }
        speed = moveSpeed.level.objectSpeed;
    }

    private void Update()
    {
        if (mode.CurrentMode == GameMode.STEP_BASED)
            return;

        if (shapeData == null)
            return;

        if (state.CurrentState == GameStates.PAUSED)
            return;

        switch (direction)
        {
            case ShapeMoveDirection.UP:
                if (transform.position.y > 6)
                {
                    Destroy(gameObject);
                }
                break;

            case ShapeMoveDirection.DOWN:
                if (transform.position.y < -6)
                {
                    Destroy(gameObject);
                }
                break;

            case ShapeMoveDirection.LEFT:
                if (transform.position.x < -10)
                {
                    Destroy(gameObject);
                }
                break;

            case ShapeMoveDirection.RIGHT:
                if (transform.position.x > 10)
                {
                    Destroy(gameObject);
                }
                break;
        }

        if (shapeData.spins == true && state.CurrentState != GameStates.PAUSED)
        {
            transform.Rotate(new Vector3(0, 0, shapeData.degreesPerSecond) * Time.deltaTime);
        }
    }

    private void FixedUpdate()
    {
        if (mode.CurrentMode == GameMode.STEP_BASED)
            return;

        if (shapeData == null)
            return;

        if (state.CurrentState == GameStates.PAUSED)
            return;

        switch (direction)
        {
            case ShapeMoveDirection.UP:
                rb.MovePosition(rb.position + Vector2.up * (speed * Time.fixedDeltaTime));
                break;

            case ShapeMoveDirection.DOWN:
                rb.MovePosition(rb.position + Vector2.down * (speed * Time.fixedDeltaTime));
                break;

            case ShapeMoveDirection.LEFT:
                rb.MovePosition(rb.position + Vector2.left * (speed * Time.fixedDeltaTime));
                break;

            case ShapeMoveDirection.RIGHT:
                rb.MovePosition(rb.position + Vector2.right * (speed * Time.fixedDeltaTime));
                break;
        }
    }

    public void Step()
    {
        if (mode.CurrentMode != GameMode.STEP_BASED)
            return;

        switch (direction)
        {
            case ShapeMoveDirection.UP:
                rb.DOMove(rb.position + Vector2.up, turnSpeed.Value);
                break;

            case ShapeMoveDirection.DOWN:
                rb.DOMove(rb.position + Vector2.down, turnSpeed.Value);
                break;

            case ShapeMoveDirection.LEFT:
                rb.DOMove(rb.position + Vector2.left, turnSpeed.Value);
                break;

            case ShapeMoveDirection.RIGHT:
                rb.DOMove(rb.position + Vector2.right, turnSpeed.Value);
                break;
        }
    }

    private void OnDestroy()
    {
        if (activeShapes.activeShapes != null && activeShapes.activeShapes.Count > 0)
            activeShapes.activeShapes.Remove(gameObject);
    }

    public void ChangeColor(Color color)
    {
        sprite.color = color;
    }

    public Color GetColor()
    {
        return sprite.color;
    }
}

public enum ShapeMoveDirection
{
    NONE, UP, DOWN, LEFT, RIGHT
}