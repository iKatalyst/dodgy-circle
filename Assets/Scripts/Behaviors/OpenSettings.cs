﻿using UnityEngine;

public class OpenSettings : MonoBehaviour
{
    public GameEvent openSettings;
    public GameEvent closeSettings;
    private bool settingsOpen;

    public void ToggleSettings()
    {
        if (settingsOpen == false)
        {
            settingsOpen = true;
            openSettings.Raise();
        }
        else
        {
            settingsOpen = false;
            closeSettings.Raise();
        }
    }
}