﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ShapeData : ScriptableObject
{
    public Sprite image;
    public bool spins;
    public float degreesPerSecond;
}