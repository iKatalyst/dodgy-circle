﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

[ExecuteInEditMode]
public class CollectablesSpawner : MonoBehaviour
{
    public IntVariable objectCount;
    public GameObject collectablePrefab;
    public CurrentLevel currentLevel;
    public GameState state;
    public GameEvent winnerEvent;

    public float intervalXMulti;
    public float intervalYMulti;

    public int spawnIntervalX;
    public int spawnIntervalY;

    public float distX;
    public float distY;

    private System.Random random;

    private void Start()
    {
        random = new System.Random(currentLevel.level.seed);
        objectCount.Value = 30;
        CalcInterval();
    }

    public void CalcInterval()
    {
        distX = Vector3.Distance(Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.scaledPixelHeight / 2, 0)), Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.scaledPixelWidth, Camera.main.scaledPixelHeight / 2, 0)));
        distY = Vector3.Distance(Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.scaledPixelWidth / 2, 0, 0)), Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.scaledPixelWidth / 2, Camera.main.scaledPixelHeight, 0)));
        spawnIntervalX = Mathf.CeilToInt(distX / intervalXMulti);
        spawnIntervalY = Mathf.CeilToInt(distY / intervalYMulti);

#if UNITY_EDITOR
        UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
#endif
    }

    public void SpawnNext()
    {
        if (objectCount.Value <= 0)
        {
            state.ChangeState(GameStates.GAME_WON);
            winnerEvent.Raise();
            return;
        }
        int randX = random.Next(-spawnIntervalX, spawnIntervalX);
        int randY = random.Next(-spawnIntervalY, spawnIntervalY);
        Instantiate(collectablePrefab, new Vector3(randX, randY, 0), Quaternion.identity);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        for (float x = -spawnIntervalX; x <= spawnIntervalX; x++)
        {
            for (float y = -spawnIntervalY; y <= spawnIntervalY; y++)
            {
                Gizmos.DrawWireSphere(new Vector3(x, y, 0), .1f);
            }
        }
    }
}