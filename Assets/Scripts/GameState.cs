﻿using UnityEngine;

[CreateAssetMenu]
public class GameState : ScriptableObject
{
    [SerializeField]
    private GameStates state;
    public GameStates CurrentState { get { return state; } }
    public GameEvent stateChangeEvent;
    private GameStates previousState;

    public void ChangeState(GameStates newState)
    {
        Debug.Log("GameState being changed to: " + newState.ToString());
        previousState = state;
        state = newState;
        stateChangeEvent.Raise();
    }

    public void ResumeFromPause()
    {
        ChangeState(previousState);
    }

    private void OnEnable()
    {
        state = GameStates.NONE;
    }
}

[System.Serializable]
public enum GameStates
{
    NONE = 0,
    PAUSED = 1,
    WAITING_TO_START = 2,
    GAME_WON = 3,
    GAME_OVER = 4,
    MOVING = 5,
    WAITING_FOR_ACTION = 6,
    STEP_END = 7
}