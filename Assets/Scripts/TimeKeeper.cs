﻿using System.Collections;
using UnityEngine;

public class TimeKeeper : MonoBehaviour
{
    public FloatVariable currentTime;
    public FloatReference maxTime;
    public GameState state;
    public GameEvent gameOver;

    private void Awake()
    {
        currentTime.Value = 0;
    }

    public void StartTimer()
    {
        currentTime.Value = 0;
        StartCoroutine(Countdown());
    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }

    private IEnumerator Countdown()
    {
        while (true)
        {
            if (state.CurrentState == GameStates.MOVING || state.CurrentState == GameStates.WAITING_FOR_ACTION)
                currentTime.Value += Time.deltaTime;
            yield return null;
        }
        //state.ChangeState(CurrentGameState.GAME_OVER);
        //gameOver.Raise();
    }
}