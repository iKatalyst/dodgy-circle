﻿using UnityEngine;

[CreateAssetMenu]
public class CurrentLevel : ScriptableObject
{
    public LevelData level;
}