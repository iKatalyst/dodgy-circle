﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ColorData : ScriptableObject
{
    public Color green;
    public Color blue;
    public Color red;
    public Color yellow;
    public Color orange;
    public Color purple;
    public Color pink;

    private List<Color> colors = new List<Color>();

    private void OnEnable()
    {
        foreach (var item in GetType().GetFields())
        {
            colors.Add((Color)item.GetValue(this));
        }
    }

    public Color GetRandomColor()
    {
        int random = Random.Range(0, colors.Count);
        return colors[random];
    }
}