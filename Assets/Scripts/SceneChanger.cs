﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public GameState state;

    public void ChangeScene(int index)
    {
        state.ChangeState(GameStates.NONE);
        SceneManager.LoadScene(index);
    }

    public void ChangeScene(string scene)
    {
        state.ChangeState(GameStates.NONE);
        SceneManager.LoadScene(scene);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}