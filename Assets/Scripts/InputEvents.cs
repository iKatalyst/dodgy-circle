﻿using UnityEngine;
using Rewired;
using RewiredConsts;

public class InputEvents : MonoBehaviour
{
    public Vector2Variable playerDirection;
    public ActiveGameMode gameMode;

    public GameState state;
    public GameEvent takeStep;
    public GameEvent startGame;
    public GameEvent backToMain;
    public GameEvent restartGame;
    public GameEvent pauseGame;
    public GameEvent unpauseGame;
    public GameEvent keyPressed;

    public InputQueue queue;

    private Rewired.Player player;

    private void Awake()
    {
        queue.CurrentInput.Clear();
    }

    private void Start()
    {
        player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        if (player.GetButtonDown(Action.CONFIRM))
        {
            keyPressed.Raise();
            if (state.CurrentState == GameStates.WAITING_TO_START)
            {
                if (gameMode.CurrentMode == GameMode.STEP_BASED)
                    state.ChangeState(GameStates.WAITING_FOR_ACTION);
                if (gameMode.CurrentMode == GameMode.REAL_TIME)
                    state.ChangeState(GameStates.MOVING);

                startGame.Raise();
                return;
            }
            if (state.CurrentState == GameStates.GAME_WON)
            {
                state.ChangeState(GameStates.NONE);
                backToMain.Raise();
                return;
            }
            if (state.CurrentState == GameStates.GAME_OVER)
            {
                state.ChangeState(GameStates.WAITING_TO_START);
                restartGame.Raise();
                return;
            }

            if (state.CurrentState == GameStates.WAITING_FOR_ACTION && gameMode.CurrentMode == GameMode.STEP_BASED)
            {
                state.ChangeState(GameStates.STEP_END);
            }
        }

        if (player.GetButtonDown(Action.CANCEL))
        {
            keyPressed.Raise();
            if ((state.CurrentState == GameStates.MOVING || state.CurrentState == GameStates.WAITING_FOR_ACTION) &&
                 state.CurrentState != GameStates.PAUSED)
            {
                state.ChangeState(GameStates.PAUSED);
                pauseGame.Raise();
                return;
            }
            if (state.CurrentState == GameStates.PAUSED)
            {
                state.ChangeState(GameStates.MOVING);
                unpauseGame.Raise();
                return;
            }
        }

        if (player.GetButtonDown(Action.TOGGLE_GAME_MODE))
        {
            if (gameMode.CurrentMode == GameMode.REAL_TIME)
            {
                gameMode.CurrentMode = GameMode.STEP_BASED;
                return;
            }
            if (gameMode.CurrentMode == GameMode.STEP_BASED)
            {
                gameMode.CurrentMode = GameMode.REAL_TIME;
                return;
            }
        }

        if (gameMode.CurrentMode == GameMode.REAL_TIME)
        {
            if (player.GetButton(Action.MOVE_RIGHT))
            {
                playerDirection.Value = Vector2.right;
                return;
            }
            if (player.GetButton(Action.MOVE_LEFT))
            {
                playerDirection.Value = Vector2.left;
                return;
            }

            if (player.GetButton(Action.MOVE_UP))
            {
                playerDirection.Value = Vector2.up;
                return;
            }
            if (player.GetButton(Action.MOVE_DOWN))
            {
                playerDirection.Value = Vector2.down;
                return;
            }

            if (playerDirection.Value != Vector2.zero)
            {
                keyPressed.Raise();
                playerDirection.Value = Vector2.zero;
            }
        }

        if (gameMode.CurrentMode == GameMode.STEP_BASED)
        {
            if (state.CurrentState == GameStates.WAITING_FOR_ACTION)
            {
                if (player.GetButtonDown(Action.MOVE_RIGHT))
                {
                    queue.CurrentInput.Add(Vector2.right);
                    keyPressed.Raise();
                }
                if (player.GetButtonDown(Action.MOVE_LEFT))
                {
                    queue.CurrentInput.Add(Vector2.left);
                    keyPressed.Raise();
                }

                if (player.GetButtonDown(Action.MOVE_UP))
                {
                    queue.CurrentInput.Add(Vector2.up);
                    keyPressed.Raise();
                }
                if (player.GetButtonDown(Action.MOVE_DOWN))
                {
                    queue.CurrentInput.Add(Vector2.down);
                    keyPressed.Raise();
                }
            }

            if (state.CurrentState == GameStates.STEP_END)
            {
                if (queue.CurrentInput.Count <= 0)
                {
                    state.ChangeState(GameStates.WAITING_FOR_ACTION);
                    return;
                }

                playerDirection.Value = queue.CurrentInput[0];
                queue.CurrentInput.RemoveAt(0);
                queue.CurrentInput.TrimExcess();
                state.ChangeState(GameStates.MOVING);
                takeStep.Raise();
            }
        }

#if UNITY_EDITOR

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            FindObjectOfType<CollectItem>().Collect();
        }
#endif
    }
}