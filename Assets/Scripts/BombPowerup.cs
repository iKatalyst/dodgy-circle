﻿using System.Collections;
using UnityEngine;
using Thinksquirrel.CShake;

public class BombPowerup : MonoBehaviour
{
    [SerializeField]
    private FloatReference explosionRadius;

    [SerializeField]
    private FloatReference expansionSpeed;

    [SerializeField]
    private FloatReference particleExpansionDivider;

    [SerializeField]
    private GameObject explosionParticles;

    [SerializeField]
    private GameObject shapeDeathParticles;

    private new CircleCollider2D collider;
    private ParticleSystem particles;

    private void Start()
    {
        collider = GetComponent<CircleCollider2D>();
        particles = explosionParticles.GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") == true)
            StartCoroutine(Explode());
        if (collision.CompareTag("Shape") == true)
        {
            if (shapeDeathParticles != null)
                Instantiate(shapeDeathParticles, collision.transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
        }
    }

    private IEnumerator Explode()
    {
        CameraShake.ShakeAll();
        gameObject.layer = SRLayers.Shape_Killer;
        particles.Play();
        var shape = particles.shape;
        do
        {
            collider.radius += expansionSpeed * Time.deltaTime;
            shape.radius += (expansionSpeed * Time.deltaTime) / particleExpansionDivider.Value;
            yield return null;
        } while (collider.radius < explosionRadius.Value);
        Destroy(gameObject);
    }
}