﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MasterLevelInfo : ScriptableObject
{
    public int currentLevelIndex;
    public List<LevelData> levels;
}