﻿using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
    public int currentIndex;
    public CurrentLevel currentLevel;
    public FloatVariable time;
    public GameState state;
    public List<LevelData> levels;

    private float spawnIntervalMultiplier;
    private float objectSpeedMultiplier;
    private float objectsPerIntervalMultiplier;

    private float objectsPerInterval;

    private void Start()
    {
        currentLevel.level = levels[currentIndex];
        state.ChangeState(GameStates.WAITING_TO_START);

        //currentLevel.level.spawnInterval = 2;
        //currentLevel.level.objectSpeed = 2;
        //currentLevel.level.objectsPerInterval = 5;
        //objectsPerInterval = currentLevel.level.objectsPerInterval;

        //spawnIntervalMultiplier = currentLevel.level.spawnInterval / time.Value;
        //objectSpeedMultiplier = currentLevel.level.objectSpeed / time.Value;
        //objectsPerIntervalMultiplier = currentLevel.level.objectsPerInterval / time.Value;
    }

    private void Update()
    {
        //if (state.CurrentState == CurrentGameState.MOVING)
        //{
        //    if (currentLevel.level.spawnInterval > 0)
        //    {
        //   }
        //    currentLevel.level.spawnInterval -= Time.deltaTime * spawnIntervalMultiplier;
        //currentLevel.level.objectSpeed += Time.deltaTime * objectSpeedMultiplier;

        //objectsPerInterval += Time.deltaTime * objectsPerIntervalMultiplier;
        //currentLevel.level.objectsPerInterval = Mathf.CeilToInt(objectsPerInterval);
        //}
    }

    public void NextRound()
    {
        currentIndex++;
        currentLevel.level = levels[currentIndex];
        state.ChangeState(GameStates.GAME_WON);
    }
}