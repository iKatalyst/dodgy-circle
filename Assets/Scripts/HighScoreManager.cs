﻿using UnityEngine;
using UnityEngine.UI;

public class HighScoreManager : MonoBehaviour
{
    public FloatVariable highScore;
    public FloatVariable time;
    public ColorData colors;
    public CanvasGroup group;
    public Image image;

    private const string scoreString = "HiScore";

    private void Awake()
    {
        highScore.Value = GetHighscore();
        if (group == null)
            return;

        if (highScore.Value != 0f)
            group.alpha = 1f;
        else
            group.alpha = 0f;
    }

    public float GetHighscore()
    {
        if (PlayerPrefs.HasKey(scoreString) == true)
        {
            return PlayerPrefs.GetFloat(scoreString);
        }
        else
        {
            return 0f;
        }
    }

    public bool HasHighscore()
    {
        return PlayerPrefs.HasKey(scoreString);
    }

    public void SetHighscore()
    {
        highScore.Value = time.Value;
        PlayerPrefs.SetFloat(scoreString, highScore.Value);
    }

    public void ChangeColor()
    {
        if (image == null || colors == null)
            return;

        image.color = colors.GetRandomColor();
    }
}