﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsController : MonoBehaviour
{
    public Transform left, right, top, bottom;

    private float camDist;

    private void Awake()
    {
        camDist = Mathf.Abs(Camera.main.transform.position.z);
        left.position = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height / 2, camDist));
        right.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height / 2, camDist));
        top.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, camDist));
        bottom.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 0, camDist));
    }
}