﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;
using Rewired;
using RewiredConsts;

public class SplashController : MonoBehaviour
{
    private TextMeshProUGUI words;
    private Rewired.Player player;

    private bool fading = false;

    private void Start()
    {
        words = GetComponent<TextMeshProUGUI>();
        player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        if (player.GetButtonDown(Action.CONFIRM) && fading == false)
        {
            fading = true;
            words.DOFade(0, 2).OnComplete(() => { SceneManager.LoadScene(1); });
        }
    }
}