﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LevelData : ScriptableObject
{
    public float spawnInterval;
    public float objectSpeed;
    public int objectsPerInterval;
    public int waveCount;
    public float spawnStaggerAmount;
    public int seed;
    public List<ShapeData> levelShapes;
}