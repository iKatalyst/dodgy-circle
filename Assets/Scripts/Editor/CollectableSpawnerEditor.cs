﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CollectablesSpawner))]
public class CollectableSpawnerEditor : Editor
{
    CollectablesSpawner self;

    private void OnEnable()
    {
        self = (CollectablesSpawner)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Update", GUILayout.Width(100)))
        {
            self.CalcInterval();
        }
    }
}