﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class MovementMarkerController : MonoBehaviour
{
    public GameState state;
    public ActiveGameMode mode;
    public InputQueue queue;
    public Transform player;
    private LineRenderer line;

    private void Start()
    {
        line = GetComponent<LineRenderer>();
    }

    public void DrawMovementLine()
    {
        if (mode.CurrentMode != GameMode.STEP_BASED)
            return;

        if (state.CurrentState == GameStates.MOVING || state.CurrentState == GameStates.WAITING_FOR_ACTION ||
            state.CurrentState == GameStates.STEP_END)
        {
            if (queue.CurrentInput == null || queue.CurrentInput.Count <= 0)
            {
                line.positionCount = 0;
                return;
            }

            line.positionCount = queue.CurrentInput.Count + 1;
            List<Vector3> locList = new List<Vector3>();

            var prevPos = player.transform.position;
            locList.Add(prevPos);
            for (int i = 0; i < queue.CurrentInput.Count; i++)
            {
                locList.Add(prevPos + queue.CurrentInput[i]);
                prevPos = prevPos + queue.CurrentInput[i];
            }

            for (int i = 0; i < locList.Count; i++)
            {
                line.SetPosition(i, locList[i]);
            }
        }
    }
}