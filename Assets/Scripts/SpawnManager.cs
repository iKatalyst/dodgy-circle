﻿using System.Collections;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Range(1, 2)]
    public int range;

    [Header("Data")]
    public ActiveGameMode mode;
    public GameState state;
    public GameObject shapePrefab;
    public CurrentLevel currentLevel;
    public ColorData colors;
    public ShapeData defaultShape;
    public GameEvent levelComplete;
    public ActiveShapes activeShapes;

    [Header("Spawn Locations")]
    public Transform leftSpawner;
    public Transform topSpawner;
    public Transform rightSpawner;
    public Transform bottomSpawner;

    private float interval;
    private float lastCheck;
    private WaitForSeconds wait;
    private System.Random random;

    private void Start()
    {
        random = new System.Random(currentLevel.level.seed);
        wait = new WaitForSeconds(currentLevel.level.spawnStaggerAmount);
        lastCheck = -10f;
        var camDist = Mathf.Abs(Camera.main.transform.position.z);
        leftSpawner.position = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height / 2, camDist)) + Vector3.left;
        rightSpawner.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height / 2, camDist)) + Vector3.right;
        topSpawner.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, camDist)) + Vector3.up; ;
        bottomSpawner.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 0, camDist) + Vector3.down);
        interval = currentLevel.level.spawnInterval;
    }

    private void Update()
    {
        if (mode.CurrentMode == GameMode.REAL_TIME || mode.CurrentMode == GameMode.MAIN_MENU)
        {
            if (mode.CurrentMode != GameMode.MAIN_MENU)
            {
                if (state.CurrentState != GameStates.MOVING && state.CurrentState != GameStates.WAITING_FOR_ACTION)
                {
                    return;
                }
            }

            if (Time.time >= lastCheck + interval)
            {
                lastCheck = Time.time;
                //currentIndex++;
                //if (currentIndex > currentLevel.level.waveCount)
                //{
                //    play = false;
                //    levelComplete.Raise();
                //    currentIndex = 0;
                //    return;
                //}
                StartCoroutine(SpawnWave());
            }
        }
    }

    public void Step()
    {
        interval--;
        if (interval <= 0)
        {
            StartCoroutine(SpawnWave());
            interval = currentLevel.level.spawnInterval;
        }
    }

    public IEnumerator SpawnWave()
    {
        for (int i = 1; i < currentLevel.level.objectsPerInterval + 1; i++)
        {
            int randNum = random.Next(-5, 6);
            ShapeMoveDirection dir = (ShapeMoveDirection)random.Next(1, 5);
            Vector3 spawnLoc = Vector3.zero; ;
            switch (dir)
            {
                case ShapeMoveDirection.UP:
                    spawnLoc = new Vector3(bottomSpawner.position.x + randNum, bottomSpawner.position.y, bottomSpawner.position.z);
                    break;

                case ShapeMoveDirection.DOWN:
                    spawnLoc = new Vector3(topSpawner.position.x + randNum, topSpawner.position.y, topSpawner.position.z);
                    break;

                case ShapeMoveDirection.LEFT:
                    spawnLoc = new Vector3(rightSpawner.position.x, rightSpawner.position.y + randNum, rightSpawner.position.z);
                    break;

                case ShapeMoveDirection.RIGHT:
                    spawnLoc = new Vector3(leftSpawner.position.x, leftSpawner.position.y + randNum, leftSpawner.position.z);
                    break;
            }
            GameObject obj = Instantiate(shapePrefab, spawnLoc, Quaternion.identity);
            var shape = obj.GetComponent<ShapeController>();
            shape.state = state;
            shape.mode = mode;
            shape.direction = dir;
            shape.moveSpeed = currentLevel;
            if (currentLevel.level.levelShapes != null && currentLevel.level.levelShapes.Count > 0)
                shape.UpdateData(currentLevel.level.levelShapes[random.Next(0, currentLevel.level.levelShapes.Count)]);
            else
                shape.UpdateData(defaultShape);
            shape.ChangeColor(colors.GetRandomColor());
            obj.AddComponent<PolygonCollider2D>();
            activeShapes.activeShapes.Add(obj);
            var parts = obj.GetComponentInChildren<ParticleSystem>().main;
            parts.startColor = shape.GetColor();
            var trail = obj.GetComponentInChildren<TrailRenderer>();
            trail.startColor = shape.GetColor();
            yield return wait;
        }
    }

    public void SetWave(LevelData level)
    {
        currentLevel.level = level;
    }

    public void DestroyActiveShapes()
    {
        foreach (var item in activeShapes.activeShapes)
        {
            Destroy(item);
        }
        activeShapes.activeShapes.Clear();
    }
}