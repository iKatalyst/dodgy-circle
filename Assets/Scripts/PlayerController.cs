﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    public ActiveGameMode gameMode;
    public Vector2Variable direction;
    public FloatVariable moveSpeed;
    public FloatVariable turnSpeed;
    public GameState state;
    public GameEvent gameOver;
    public GameEvent stepEnd;
    public GameObject deathParticles;
    public IntVariable mouseControl;
    public bool godMode;
    private Rigidbody2D rb;
    private float camDist;
    //private Vector2 mouseLoc;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        camDist = Mathf.Abs(Camera.main.transform.position.z);
        direction.Value = new Vector2(0, 0);
    }

    private void Update()
    {
        if (state.CurrentState == GameStates.PAUSED)
            return;

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.G) && Application.isEditor == true)
            godMode = !godMode;

        // mouseLoc = Input.mousePosition;
    }

    public void Step()
    {
        rb.DOMove(rb.position + direction.Value, turnSpeed.Value).OnComplete(() =>
        {
            if (gameMode.CurrentMode == GameMode.STEP_BASED)
            {
                stepEnd.Raise();
                state.ChangeState(GameStates.STEP_END);
                return;
            }
            if (gameMode.CurrentMode == GameMode.REAL_TIME)
            {
                state.ChangeState(GameStates.WAITING_FOR_ACTION);
            }
        });
    }

    private void FixedUpdate()
    {
        if (gameMode.CurrentMode != GameMode.REAL_TIME)
            return;

        if (state.CurrentState == GameStates.PAUSED)
            return;

        //if (mouseControl.Value == 0)
        rb.MovePosition(rb.position + direction.Value * (moveSpeed.Value * Time.fixedDeltaTime));
        //else
        //    rb.MovePosition(Camera.main.ScreenToWorldPoint(new Vector3(mouseLoc.x, mouseLoc.y, camDist)));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Shape"))
        {
            if (godMode == true)
            {
                Destroy(collision.gameObject);
                return;
            }
            state.ChangeState(GameStates.GAME_OVER);
            Instantiate(deathParticles, transform.position, Quaternion.identity);
            gameOver.Raise();
            Destroy(gameObject);
        }
    }
}