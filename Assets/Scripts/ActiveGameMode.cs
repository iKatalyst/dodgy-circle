﻿using UnityEngine;

[CreateAssetMenu]
public class ActiveGameMode : ScriptableObject
{
    [SerializeField]
    private GameMode mode;
    public GameMode CurrentMode
    {
        get { return mode; }
        set { mode = value; }
    }
}

public enum GameMode
{
    REAL_TIME = 0,
    STEP_BASED = 1,
    MAIN_MENU = 2
}