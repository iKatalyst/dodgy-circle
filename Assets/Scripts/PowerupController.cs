using UnityEngine;
using System.Collections;

public class PowerupController : MonoBehaviour
{
}

public class Powerup : ScriptableObject
{
    public GameObject particle;
    public float weight;
}