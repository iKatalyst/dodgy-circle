﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ActiveShapes : ScriptableObject
{
    public List<GameObject> activeShapes = new List<GameObject>();
}