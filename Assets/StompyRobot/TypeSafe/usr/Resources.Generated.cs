// ------------------------------------------------------------------------------
//  _______   _____ ___ ___   _   ___ ___ 
// |_   _\ \ / / _ \ __/ __| /_\ | __| __|
//   | |  \ V /|  _/ _|\__ \/ _ \| _|| _| 
//   |_|   |_| |_| |___|___/_/ \_\_| |___|
// 
// This file has been generated automatically by TypeSafe.
// Any changes to this file may be lost when it is regenerated.
// https://www.stompyrobot.uk/tools/typesafe
// 
// TypeSafe Version: 1.3.2-Unity5
// 
// ------------------------------------------------------------------------------



public sealed class SRResources {
    
    private SRResources() {
    }
    
    private const string _tsInternal = "1.3.2-Unity5";
    
    public static global::TypeSafe.Resource<global::UnityEngine.TextAsset> LineBreaking_Leading_Characters {
        get {
            return ((global::TypeSafe.Resource<global::UnityEngine.TextAsset>)(__ts_internal_resources[0]));
        }
    }
    
    public static global::TypeSafe.Resource<global::DG.Tweening.Core.DOTweenSettings> DOTweenSettings {
        get {
            return ((global::TypeSafe.Resource<global::DG.Tweening.Core.DOTweenSettings>)(__ts_internal_resources[1]));
        }
    }
    
    public static global::TypeSafe.Resource<global::UnityEngine.TextAsset> LineBreaking_Following_Characters {
        get {
            return ((global::TypeSafe.Resource<global::UnityEngine.TextAsset>)(__ts_internal_resources[2]));
        }
    }
    
    private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_resources = new global::System.Collections.ObjectModel.ReadOnlyCollection<global::TypeSafe.IResource>(new global::TypeSafe.IResource[] {
                new global::TypeSafe.Resource<global::UnityEngine.TextAsset>("LineBreaking Leading Characters", "LineBreaking Leading Characters"),
                new global::TypeSafe.Resource<global::DG.Tweening.Core.DOTweenSettings>("DOTweenSettings", "DOTweenSettings"),
                new global::TypeSafe.Resource<global::UnityEngine.TextAsset>("LineBreaking Following Characters", "LineBreaking Following Characters")});
    
    public sealed class Fonts_Materials {
        
        private Fonts_Materials() {
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> Anton_SDF_Drop_Shadow {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[0]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> Anton_SDF_Outline {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[1]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> LiberationSans_SDF_Drop_Shadow {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[2]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> Bangers_SDF_Logo {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[3]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> LiberationSans_SDF_Metalic_Green {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[4]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> LiberationSans_SDF_Soft_Mask {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[5]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> LiberationSans_SDF_Overlay {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[6]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> RobotoBold_SDF_Surface {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[7]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> Bangers_SDF_Outline {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[8]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> RobotoBold_SDF_Drop_Shadow {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[9]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> Bangers_SDF_Glow {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[10]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> Bangers_SDF_Drop_Shadow {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[11]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Material> LiberationSans_SDF_Outline {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Material>)(__ts_internal_resources[12]));
            }
        }
        
        private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_resources = new global::System.Collections.ObjectModel.ReadOnlyCollection<global::TypeSafe.IResource>(new global::TypeSafe.IResource[] {
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Anton SDF - Drop Shadow", "Fonts & Materials/Anton SDF - Drop Shadow"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Anton SDF - Outline", "Fonts & Materials/Anton SDF - Outline"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("LiberationSans SDF - Drop Shadow", "Fonts & Materials/LiberationSans SDF - Drop Shadow"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Bangers SDF Logo", "Fonts & Materials/Bangers SDF Logo"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("LiberationSans SDF - Metalic Green", "Fonts & Materials/LiberationSans SDF - Metalic Green"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("LiberationSans SDF - Soft Mask", "Fonts & Materials/LiberationSans SDF - Soft Mask"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("LiberationSans SDF - Overlay", "Fonts & Materials/LiberationSans SDF - Overlay"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Roboto-Bold SDF - Surface", "Fonts & Materials/Roboto-Bold SDF - Surface"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Bangers SDF - Outline", "Fonts & Materials/Bangers SDF - Outline"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Roboto-Bold SDF - Drop Shadow", "Fonts & Materials/Roboto-Bold SDF - Drop Shadow"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Bangers SDF Glow", "Fonts & Materials/Bangers SDF Glow"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("Bangers SDF - Drop Shadow", "Fonts & Materials/Bangers SDF - Drop Shadow"),
                    new global::TypeSafe.Resource<global::UnityEngine.Material>("LiberationSans SDF - Outline", "Fonts & Materials/LiberationSans SDF - Outline")});
        
        /// <summary>
        /// Return a list of all resources in this folder.
        /// This method has a very low performance cost, no need to cache the result.
        /// </summary>
        /// <returns>A list of resource objects in this folder.</returns>
        public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContents() {
            return __ts_internal_resources;
        }
        
        private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_recursiveLookupCache;
        
        /// <summary>
        /// Return a list of all resources in this folder and all sub-folders.
        /// The result of this method is cached, so subsequent calls will have very low performance cost.
        /// </summary>
        /// <returns>A list of resource objects in this folder and sub-folders.</returns>
        public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContentsRecursive() {
            if ((__ts_internal_recursiveLookupCache != null)) {
                return __ts_internal_recursiveLookupCache;
            }
            global::System.Collections.Generic.List<global::TypeSafe.IResource> tmp = new global::System.Collections.Generic.List<global::TypeSafe.IResource>();
            tmp.AddRange(GetContents());
            __ts_internal_recursiveLookupCache = tmp;
            return __ts_internal_recursiveLookupCache;
        }
        
        /// <summary>
        /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref> (does not include sub-folders)
        /// This method does not cache the result, so you should cache the result yourself if you will use it often.
        /// </summary>
        /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder.</returns>
        public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContents<TResource>()
            where TResource : global::UnityEngine.Object {
            return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContents());
        }
        
        /// <summary>
        /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref>, including sub-folders.
        /// This method does not cache the result, so you should cache the result yourself if you will use it often.
        /// </summary>
        /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder and sub-folders.</returns>
        public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContentsRecursive<TResource>()
            where TResource : global::UnityEngine.Object {
            return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContentsRecursive());
        }
        
        /// <summary>
        /// Call Unload() on every loaded resource in this folder.
        /// </summary>
        public static void UnloadAll() {
            global::TypeSafe.TypeSafeUtil.UnloadAll(GetContents());
        }
        
        /// <summary>
        /// Call Unload() on every loaded resource in this folder and subfolders.
        /// </summary>
        private void UnloadAllRecursive() {
            global::TypeSafe.TypeSafeUtil.UnloadAll(GetContentsRecursive());
        }
    }
    
    public sealed class Shaders {
        
        private Shaders() {
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_BitmapMobile {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[0]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.TextAsset> TMPro_Surface {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.TextAsset>)(__ts_internal_resources[1]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDFSurface {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[2]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDF {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[3]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDFMobile_Overlay {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[4]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.TextAsset> TMPro_Properties {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.TextAsset>)(__ts_internal_resources[5]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDFSurfaceMobile {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[6]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDF_Overlay {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[7]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.TextAsset> TMPro {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.TextAsset>)(__ts_internal_resources[8]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDFMobile_Masking {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[9]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_Bitmap {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[10]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_Sprite {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[11]));
            }
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> TMP_SDFMobile {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[12]));
            }
        }
        
        private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_resources = new global::System.Collections.ObjectModel.ReadOnlyCollection<global::TypeSafe.IResource>(new global::TypeSafe.IResource[] {
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_Bitmap-Mobile", "Shaders/TMP_Bitmap-Mobile"),
                    new global::TypeSafe.Resource<global::UnityEngine.TextAsset>("TMPro_Surface", "Shaders/TMPro_Surface"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF-Surface", "Shaders/TMP_SDF-Surface"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF", "Shaders/TMP_SDF"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF-Mobile Overlay", "Shaders/TMP_SDF-Mobile Overlay"),
                    new global::TypeSafe.Resource<global::UnityEngine.TextAsset>("TMPro_Properties", "Shaders/TMPro_Properties"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF-Surface-Mobile", "Shaders/TMP_SDF-Surface-Mobile"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF Overlay", "Shaders/TMP_SDF Overlay"),
                    new global::TypeSafe.Resource<global::UnityEngine.TextAsset>("TMPro", "Shaders/TMPro"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF-Mobile Masking", "Shaders/TMP_SDF-Mobile Masking"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_Bitmap", "Shaders/TMP_Bitmap"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_Sprite", "Shaders/TMP_Sprite"),
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("TMP_SDF-Mobile", "Shaders/TMP_SDF-Mobile")});
        
        /// <summary>
        /// Return a list of all resources in this folder.
        /// This method has a very low performance cost, no need to cache the result.
        /// </summary>
        /// <returns>A list of resource objects in this folder.</returns>
        public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContents() {
            return __ts_internal_resources;
        }
        
        private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_recursiveLookupCache;
        
        /// <summary>
        /// Return a list of all resources in this folder and all sub-folders.
        /// The result of this method is cached, so subsequent calls will have very low performance cost.
        /// </summary>
        /// <returns>A list of resource objects in this folder and sub-folders.</returns>
        public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContentsRecursive() {
            if ((__ts_internal_recursiveLookupCache != null)) {
                return __ts_internal_recursiveLookupCache;
            }
            global::System.Collections.Generic.List<global::TypeSafe.IResource> tmp = new global::System.Collections.Generic.List<global::TypeSafe.IResource>();
            tmp.AddRange(GetContents());
            __ts_internal_recursiveLookupCache = tmp;
            return __ts_internal_recursiveLookupCache;
        }
        
        /// <summary>
        /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref> (does not include sub-folders)
        /// This method does not cache the result, so you should cache the result yourself if you will use it often.
        /// </summary>
        /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder.</returns>
        public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContents<TResource>()
            where TResource : global::UnityEngine.Object {
            return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContents());
        }
        
        /// <summary>
        /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref>, including sub-folders.
        /// This method does not cache the result, so you should cache the result yourself if you will use it often.
        /// </summary>
        /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder and sub-folders.</returns>
        public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContentsRecursive<TResource>()
            where TResource : global::UnityEngine.Object {
            return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContentsRecursive());
        }
        
        /// <summary>
        /// Call Unload() on every loaded resource in this folder.
        /// </summary>
        public static void UnloadAll() {
            global::TypeSafe.TypeSafeUtil.UnloadAll(GetContents());
        }
        
        /// <summary>
        /// Call Unload() on every loaded resource in this folder and subfolders.
        /// </summary>
        private void UnloadAllRecursive() {
            global::TypeSafe.TypeSafeUtil.UnloadAll(GetContentsRecursive());
        }
    }
    
    public sealed class Sharpen {
        
        private Sharpen() {
        }
        
        public static global::TypeSafe.Resource<global::UnityEngine.Shader> _Sharpen {
            get {
                return ((global::TypeSafe.Resource<global::UnityEngine.Shader>)(__ts_internal_resources[0]));
            }
        }
        
        private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_resources = new global::System.Collections.ObjectModel.ReadOnlyCollection<global::TypeSafe.IResource>(new global::TypeSafe.IResource[] {
                    new global::TypeSafe.Resource<global::UnityEngine.Shader>("_Sharpen", "Sharpen/Sharpen")});
        
        /// <summary>
        /// Return a list of all resources in this folder.
        /// This method has a very low performance cost, no need to cache the result.
        /// </summary>
        /// <returns>A list of resource objects in this folder.</returns>
        public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContents() {
            return __ts_internal_resources;
        }
        
        private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_recursiveLookupCache;
        
        /// <summary>
        /// Return a list of all resources in this folder and all sub-folders.
        /// The result of this method is cached, so subsequent calls will have very low performance cost.
        /// </summary>
        /// <returns>A list of resource objects in this folder and sub-folders.</returns>
        public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContentsRecursive() {
            if ((__ts_internal_recursiveLookupCache != null)) {
                return __ts_internal_recursiveLookupCache;
            }
            global::System.Collections.Generic.List<global::TypeSafe.IResource> tmp = new global::System.Collections.Generic.List<global::TypeSafe.IResource>();
            tmp.AddRange(GetContents());
            __ts_internal_recursiveLookupCache = tmp;
            return __ts_internal_recursiveLookupCache;
        }
        
        /// <summary>
        /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref> (does not include sub-folders)
        /// This method does not cache the result, so you should cache the result yourself if you will use it often.
        /// </summary>
        /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder.</returns>
        public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContents<TResource>()
            where TResource : global::UnityEngine.Object {
            return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContents());
        }
        
        /// <summary>
        /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref>, including sub-folders.
        /// This method does not cache the result, so you should cache the result yourself if you will use it often.
        /// </summary>
        /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder and sub-folders.</returns>
        public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContentsRecursive<TResource>()
            where TResource : global::UnityEngine.Object {
            return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContentsRecursive());
        }
        
        /// <summary>
        /// Call Unload() on every loaded resource in this folder.
        /// </summary>
        public static void UnloadAll() {
            global::TypeSafe.TypeSafeUtil.UnloadAll(GetContents());
        }
        
        /// <summary>
        /// Call Unload() on every loaded resource in this folder and subfolders.
        /// </summary>
        private void UnloadAllRecursive() {
            global::TypeSafe.TypeSafeUtil.UnloadAll(GetContentsRecursive());
        }
    }
    
    /// <summary>
    /// Return a list of all resources in this folder.
    /// This method has a very low performance cost, no need to cache the result.
    /// </summary>
    /// <returns>A list of resource objects in this folder.</returns>
    public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContents() {
        return __ts_internal_resources;
    }
    
    private static global::System.Collections.Generic.IList<global::TypeSafe.IResource> __ts_internal_recursiveLookupCache;
    
    /// <summary>
    /// Return a list of all resources in this folder and all sub-folders.
    /// The result of this method is cached, so subsequent calls will have very low performance cost.
    /// </summary>
    /// <returns>A list of resource objects in this folder and sub-folders.</returns>
    public static global::System.Collections.Generic.IList<global::TypeSafe.IResource> GetContentsRecursive() {
        if ((__ts_internal_recursiveLookupCache != null)) {
            return __ts_internal_recursiveLookupCache;
        }
        global::System.Collections.Generic.List<global::TypeSafe.IResource> tmp = new global::System.Collections.Generic.List<global::TypeSafe.IResource>();
        tmp.AddRange(GetContents());
        tmp.AddRange(Fonts_Materials.GetContentsRecursive());
        tmp.AddRange(Shaders.GetContentsRecursive());
        tmp.AddRange(Sharpen.GetContentsRecursive());
        __ts_internal_recursiveLookupCache = tmp;
        return __ts_internal_recursiveLookupCache;
    }
    
    /// <summary>
    /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref> (does not include sub-folders)
    /// This method does not cache the result, so you should cache the result yourself if you will use it often.
    /// </summary>
    /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder.</returns>
    public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContents<TResource>()
        where TResource : global::UnityEngine.Object {
        return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContents());
    }
    
    /// <summary>
    /// Return a list of all resources in this folder of type <typeparamref>TResource</typeparamref>, including sub-folders.
    /// This method does not cache the result, so you should cache the result yourself if you will use it often.
    /// </summary>
    /// <returns>A list of <typeparamref>TResource</typeparamref> objects in this folder and sub-folders.</returns>
    public static global::System.Collections.Generic.List<global::TypeSafe.Resource<TResource>> GetContentsRecursive<TResource>()
        where TResource : global::UnityEngine.Object {
        return global::TypeSafe.TypeSafeUtil.GetResourcesOfType<TResource>(GetContentsRecursive());
    }
    
    /// <summary>
    /// Call Unload() on every loaded resource in this folder.
    /// </summary>
    public static void UnloadAll() {
        global::TypeSafe.TypeSafeUtil.UnloadAll(GetContents());
    }
    
    /// <summary>
    /// Call Unload() on every loaded resource in this folder and subfolders.
    /// </summary>
    private void UnloadAllRecursive() {
        global::TypeSafe.TypeSafeUtil.UnloadAll(GetContentsRecursive());
    }
}
